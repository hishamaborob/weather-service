## Weather Service

##### Important Note:
- This is a quick 2.5 hours work I could afford that’s intended as a POC. Good structure and best practices are taken into account but overall it shouldn’t be treated as a production ready solution.
- Validation rules and error/exception handling are very basic and some are ignored intentionally.
- Unit and integration tests don't provide very wide coverage, only main functionalities and cases are tested.
- Numbers precession, reliable configuration and security rules are all ignored.

##### Overview

- A simple REST API based on Java 8, Spring Boot and Maven.
- It uses an external API to get weather data.
- It provides the endpoint /v1/weather/current to query the current weather of a location.
- It caches the last 5 queries for a particular location to be aggregated and retrieved through this endpoint /v1/weather/historical.
- I use DTO for projection.
- Some Unit and Integration test provided. (Not 100% test coverage)
- Integration test simulate the external weather API web server.
- Swagger UI can be used to test the API.

##### Production Ready Service
A production ready service is in short words: is a reliable service.
A reliable service is the one that can scale up well and be available. 
In our case, a weather service should handle high traffic, scales horizontally and be highly available.
We should handle a situation where dependency like the external weather API goes down. Maybe cache some responses.
A reliable service should also be observable, code is clear, readable, and maintainable, and changes are tracked.
A reliable service should be monitored well, and enough logs should be collected, and alerts should be setup.
A reliable service is tested well, unite and integration test with a full or near full coverage.
A reliable service should also have a clear and easy way to deploy changes.

##### Deploy the Service
I would define the infrastructure and tools needed using an Infrastructure as Code tool like CDK or Terraform.
I will use Docker to run the service on AWS ECS Fargate, Kubernetes, or ElasticBeanStalk.
The CI will be AWS CodePipeline that upon caching Git hooks, it takes care of building docker images, store them and push 
them to the new instances.
To achieve zero down time deployment, we can setup health check and blue-green deployment.

#### Build and Run


In the parent directory:
```
mvn clean package
```

Run:
```
java -jar target/*.jar
```

Location:
```
http://localhost:8080/swagger-ui.html
```