package sample.weather.client;

import okhttp3.*;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import sample.weather.client.exception.ServiceClientErrorException;
import sample.weather.client.exception.ServiceClientNoDataFoundException;

import java.io.IOException;
import java.util.Map;

public abstract class ServiceClient {

    private static final Logger LOGGER = Logger.getLogger(ServiceClient.class);

    private static final String APP_ID_PARAM = "APPID";
    private final OkHttpClient client;
    private final String baseUrl;
    private final String appId;

    public ServiceClient(OkHttpClient client, String baseUrl, String appId) {

        this.client = client;
        this.baseUrl = baseUrl;
        this.appId = appId;
    }

    protected JSONObject executeGet(final String path, Map<String, String> parameters)
            throws ServiceClientErrorException, ServiceClientNoDataFoundException {

        HttpUrl.Builder urlBuilder = HttpUrl.parse(baseUrl + "/" + path).newBuilder();
        parameters.forEach((k, v) -> urlBuilder.addQueryParameter(k, v));
        urlBuilder.addQueryParameter(APP_ID_PARAM, appId);
        String url = urlBuilder.build().toString();
        Request request = new Request.Builder().url(url).build();
        return executeRequest(request);
    }

    private JSONObject executeRequest(Request request)
            throws ServiceClientErrorException, ServiceClientNoDataFoundException {

        JSONObject jsonObject = null;
        try (Response response = client.newCall(request).execute()) {
            if (response.code() == 404) {
                throw new ServiceClientNoDataFoundException("No data was found");
            }
            if (!response.isSuccessful()) {
                throw new ServiceClientErrorException("Could not retrieve data");
            }
            String responseData = response.body().string();
            jsonObject = new JSONObject(responseData);
        } catch (IOException e) {
            LOGGER.error(e);
            throw new ServiceClientErrorException(e.getMessage(), e);
        } catch (JSONException e) {
            LOGGER.warn(e);
            throw new ServiceClientErrorException(e.getMessage(), e);
        }
        return jsonObject;
    }
}
