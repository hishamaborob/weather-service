package sample.weather.client;

import okhttp3.OkHttpClient;
import org.json.JSONObject;
import sample.weather.client.exception.ServiceClientErrorException;
import sample.weather.client.exception.ServiceClientNoDataFoundException;

import java.util.HashMap;
import java.util.Map;

public class WeatherServiceClient extends ServiceClient {

    private static final String PATH = "weather";

    public WeatherServiceClient(OkHttpClient client, String baseUrl, String appId) {
        super(client, baseUrl, appId);
    }

    /**
     * Simple get current weather with two main parameters
     *
     * @param location
     * @param unit    Fahrenheit, Celsius and Kelvin
     * @return JSONObject
     * @throws ServiceClientErrorException
     * @throws ServiceClientNoDataFoundException
     */
    public JSONObject getCurrentWeather(final String location, final String unit)
            throws ServiceClientErrorException, ServiceClientNoDataFoundException {

        Map<String, String> params = new HashMap<>();
        params.put("q", location);
        params.put("units", unit);
        return executeGet(PATH, params);
    }
}
