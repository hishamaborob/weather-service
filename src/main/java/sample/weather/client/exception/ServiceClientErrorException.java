package sample.weather.client.exception;

public class ServiceClientErrorException extends RuntimeException {

    public ServiceClientErrorException(String message) {
        super(message);
    }

    public ServiceClientErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
