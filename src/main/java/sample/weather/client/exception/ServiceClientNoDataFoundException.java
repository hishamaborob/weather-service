package sample.weather.client.exception;

public class ServiceClientNoDataFoundException extends RuntimeException {

    public ServiceClientNoDataFoundException(String message) {
        super(message);
    }

    public ServiceClientNoDataFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
