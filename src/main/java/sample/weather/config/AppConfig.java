package sample.weather.config;

import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sample.weather.client.WeatherServiceClient;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Configuration
public class AppConfig {

    @Value("${weather.service.base.url}")
    private String weatherServiceBaseUrl;

    @Value("${weather.service.app.id}")
    private String weatherServiceAppId;

    @Bean
    public ExecutorService executorService() {

        return Executors.newSingleThreadExecutor();
    }

    @Bean
    public OkHttpClient httpClient() {

        return new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .build();
    }

    @Bean("weatherServiceClient")
    public WeatherServiceClient weatherServiceClient(OkHttpClient okHttpClient) {

        return new WeatherServiceClient(okHttpClient, weatherServiceBaseUrl, weatherServiceAppId);
    }
}
