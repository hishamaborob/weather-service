package sample.weather.controller;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sample.weather.dto.WeatherHistoricalDTO;
import sample.weather.dto.WeatherSummaryDTO;
import sample.weather.model.Weather;
import sample.weather.model.WeatherHistoricalInfo;
import sample.weather.service.*;
import sample.weather.service.exception.NoDataFoundException;
import sample.weather.service.exception.ServiceErrorException;

import static sample.weather.helper.WeatherDataHelper.convertToDto;

@RestController
@RequestMapping("/v1/weather")
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private WeatherForecastService weatherForecastService;

    @GetMapping("/current")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Current Weather Summary Data"),
            @ApiResponse(code = 404, message = "Location not found"),
            @ApiResponse(code = 500, message = "service error")
    })
    public ResponseEntity<WeatherSummaryDTO> getCurrentWeatherSummary(
            @RequestParam(name = "location") String location) {

        Weather weather = null;
        try {
            weather = weatherService.getCurrentWeather(location.toLowerCase());
        } catch (NoDataFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (ServiceErrorException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (weather == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(convertToDto(weather), HttpStatus.OK);
    }

    @GetMapping("/historical")
    @ResponseBody
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Weather Historical Data"),
            @ApiResponse(code = 404, message = "Location not found")
    })
    public ResponseEntity<WeatherHistoricalDTO> getWeatherHistoricalData(
            @RequestParam(name = "location") String location) {

        WeatherHistoricalInfo weatherHistoricalInfo = null;
        try {
            weatherHistoricalInfo =
                    weatherForecastService.getWeatherHistoricalData(location.toLowerCase());
        } catch (NoDataFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(convertToDto(weatherHistoricalInfo), HttpStatus.OK);
    }
}
