package sample.weather.dto;

import java.util.List;
import java.util.Objects;

public class WeatherHistoricalDTO {

    private int avgTemp;
    private int avgPressure;
    private List<WeatherSummaryDTO> history;

    public int getAvgTemp() {
        return avgTemp;
    }

    public void setAvgTemp(int avgTemp) {
        this.avgTemp = avgTemp;
    }

    public int getAvgPressure() {
        return avgPressure;
    }

    public void setAvgPressure(int avgPressure) {
        this.avgPressure = avgPressure;
    }

    public List<WeatherSummaryDTO> getHistory() {
        return history;
    }

    public void setHistory(List<WeatherSummaryDTO> history) {
        this.history = history;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherHistoricalDTO that = (WeatherHistoricalDTO) o;
        return avgTemp == that.avgTemp &&
                avgPressure == that.avgPressure &&
                Objects.equals(history, that.history);
    }

    @Override
    public int hashCode() {
        return Objects.hash(avgTemp, avgPressure, history);
    }
}
