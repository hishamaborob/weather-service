package sample.weather.dto;

import java.util.Objects;

public class WeatherSummaryDTO {

    private int temp;
    private int pressure;
    private boolean umbrella;

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public boolean isUmbrella() {
        return umbrella;
    }

    public void setUmbrella(boolean umbrella) {
        this.umbrella = umbrella;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherSummaryDTO that = (WeatherSummaryDTO) o;
        return temp == that.temp &&
                pressure == that.pressure &&
                umbrella == that.umbrella;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temp, pressure, umbrella);
    }
}
