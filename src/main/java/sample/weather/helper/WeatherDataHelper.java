package sample.weather.helper;

import sample.weather.dto.WeatherHistoricalDTO;
import sample.weather.dto.WeatherSummaryDTO;
import sample.weather.model.Weather;
import sample.weather.model.WeatherHistoricalInfo;

import java.util.stream.Collectors;

public class WeatherDataHelper {

    private WeatherDataHelper() {
    }

    public static WeatherSummaryDTO convertToDto(Weather weather) {

        WeatherSummaryDTO weatherSummaryDTO = new WeatherSummaryDTO();
        weatherSummaryDTO.setTemp((int) weather.getTemperature());
        weatherSummaryDTO.setPressure((int) weather.getPressure());
        weatherSummaryDTO.setUmbrella(weather.isNeedUmbrella());
        return weatherSummaryDTO;
    }

    public static WeatherHistoricalDTO convertToDto(WeatherHistoricalInfo weatherHistoricalInfo) {

        WeatherHistoricalDTO weatherHistoricalDTO = new WeatherHistoricalDTO();
        weatherHistoricalDTO.setAvgTemp((int) weatherHistoricalInfo.getAverageTemperature());
        weatherHistoricalDTO.setAvgPressure((int) weatherHistoricalInfo.getAveragePressure());
        weatherHistoricalDTO.setHistory(weatherHistoricalInfo.getHistory()
                .stream().map(WeatherDataHelper::convertToDto)
                .collect(Collectors.toList()));
        return weatherHistoricalDTO;
    }
}
