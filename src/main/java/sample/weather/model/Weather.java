package sample.weather.model;

import java.util.Objects;

public class Weather {

    private final double temperature;
    private final double pressure;
    private final boolean needUmbrella;

    public Weather(double temperature, double pressure, boolean needUmbrella) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.needUmbrella = needUmbrella;
    }

    public double getTemperature() {
        return temperature;
    }

    public double getPressure() {
        return pressure;
    }

    public boolean isNeedUmbrella() {
        return needUmbrella;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Weather weather = (Weather) o;
        return temperature == weather.temperature &&
                pressure == weather.pressure &&
                needUmbrella == weather.needUmbrella;
    }

    @Override
    public int hashCode() {
        return Objects.hash(temperature, pressure, needUmbrella);
    }
}
