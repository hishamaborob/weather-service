package sample.weather.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class WeatherHistoricalInfo {

    private final double averageTemperature;
    private final double averagePressure;
    private final List<Weather> history;

    public WeatherHistoricalInfo(double averageTemperature, double averagePressure, List<Weather> history) {
        this.averageTemperature = averageTemperature;
        this.averagePressure = averagePressure;
        this.history = history;
    }

    public double getAverageTemperature() {
        return averageTemperature;
    }

    public double getAveragePressure() {
        return averagePressure;
    }

    public List<Weather> getHistory() {
        return new ArrayList<>(history);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeatherHistoricalInfo that = (WeatherHistoricalInfo) o;
        return averageTemperature == that.averageTemperature &&
                averagePressure == that.averagePressure &&
                Objects.equals(history, that.history);
    }

    @Override
    public int hashCode() {
        return Objects.hash(averageTemperature, averagePressure, history);
    }
}
