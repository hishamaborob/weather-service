package sample.weather.repository;

import sample.weather.model.Weather;

import java.util.List;

public interface WeatherHistoricalInfoRepository {

    public void add(final String location, Weather weather);

    public List<Weather> get(final String location);
}
