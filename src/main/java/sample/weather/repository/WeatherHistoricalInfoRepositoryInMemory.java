package sample.weather.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import sample.weather.model.Weather;

import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class WeatherHistoricalInfoRepositoryInMemory implements WeatherHistoricalInfoRepository {

    private int cacheMaxSize;

    Map<String, LinkedList<Weather>> cache = new ConcurrentHashMap<>();

    public WeatherHistoricalInfoRepositoryInMemory(@Value("${cache.max.size}") int cacheMaxSize) {
        this.cacheMaxSize = cacheMaxSize;
    }

    @Override
    public void add(String location, Weather weather) {

        if (!cache.containsKey(location)) {
            cache.put(location, new LinkedList<>());
        }

        synchronized (this) {
            LinkedList<Weather> locationData = cache.get(location);
            if (locationData.size() == cacheMaxSize) {
                locationData.removeFirst();
            }
            locationData.addLast(weather);
        }
    }

    @Override
    public LinkedList<Weather> get(String location) {


        return cache.getOrDefault(location, new LinkedList<>());
    }
}
