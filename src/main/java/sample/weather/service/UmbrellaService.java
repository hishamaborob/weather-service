package sample.weather.service;

import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UmbrellaService {

    private static final Set<String> keyWords = new HashSet<>();

    {
        keyWords.add("thunderstorm");
        keyWords.add("drizzle");
        keyWords.add("rain");
    }

    public boolean shouldTakeUmbrella(final String text) {

        return keyWords.contains(text.toLowerCase());
    }
}
