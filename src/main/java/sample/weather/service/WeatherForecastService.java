package sample.weather.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sample.weather.model.Weather;
import sample.weather.model.WeatherHistoricalInfo;
import sample.weather.repository.WeatherHistoricalInfoRepository;
import sample.weather.service.exception.NoDataFoundException;

import java.util.List;
import java.util.concurrent.ExecutorService;

import static java.lang.String.format;

@Service
public class WeatherForecastService {

    private WeatherHistoricalInfoRepository weatherHistoricalInfoRepository;
    private ExecutorService executorService;

    @Autowired
    public WeatherForecastService(
            WeatherHistoricalInfoRepository weatherHistoricalInfoRepository, ExecutorService executorService) {

        this.weatherHistoricalInfoRepository = weatherHistoricalInfoRepository;
        this.executorService = executorService;
    }

    public void addWeatherData(final String location, Weather weather) {

        executorService.submit(() -> {
            weatherHistoricalInfoRepository.add(location, weather);
        });
    }

    public WeatherHistoricalInfo getWeatherHistoricalData(final String location) throws NoDataFoundException {

        WeatherHistoricalInfo weatherHistoricalInfo = null;
        List<Weather> weatherList = weatherHistoricalInfoRepository.get(location);
        if (weatherList.isEmpty()) {
            throw new NoDataFoundException(format("Location %s not found", location));
        }
        weatherHistoricalInfo = new WeatherHistoricalInfo(
                getAverageTemperature(weatherList), getAveragePressure(weatherList), weatherList
        );
        return weatherHistoricalInfo;
    }

    private double getAverageTemperature(List<Weather> weatherList) {

        return weatherList.stream().mapToDouble(weather -> weather.getTemperature()).average().orElse(0);
    }

    private double getAveragePressure(List<Weather> weatherList) {

        return weatherList.stream().mapToDouble(weather -> weather.getPressure()).average().orElse(0);
    }
}
