package sample.weather.service;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import sample.weather.client.*;
import sample.weather.client.exception.ServiceClientErrorException;
import sample.weather.client.exception.ServiceClientNoDataFoundException;
import sample.weather.model.Weather;
import sample.weather.service.exception.NoDataFoundException;
import sample.weather.service.exception.ServiceErrorException;

import static java.lang.String.format;

@Service
public class WeatherService {

    private static final Logger LOGGER = Logger.getLogger(ServiceClient.class);

    //celsius .. currently static. this service return only celsius
    private static final String TEMP_UNIT = "metric";
    private WeatherServiceClient weatherServiceClient;
    private UmbrellaService umbrellaService;
    private WeatherForecastService weatherForecastService;

    @Autowired
    public WeatherService(
            @Qualifier("weatherServiceClient") WeatherServiceClient weatherServiceClient,
            UmbrellaService umbrellaService, WeatherForecastService weatherForecastService) {
        this.weatherServiceClient = weatherServiceClient;
        this.umbrellaService = umbrellaService;
        this.weatherForecastService = weatherForecastService;
    }

    public Weather getCurrentWeather(final String location) {

        Weather weather = null;
        try {
            JSONObject jsonResponse = weatherServiceClient.getCurrentWeather(location, TEMP_UNIT);
            if (jsonResponse == null) {
                throw new NoDataFoundException("An error occurred or wrong params");
            }
            JSONObject main = jsonResponse.getJSONObject("main");
            final String weatherDescription =
                    jsonResponse.getJSONArray("weather").getJSONObject(0).getString("main");
            weather = new Weather(
                    main.getDouble("temp"), main.getDouble("pressure"),
                    umbrellaService.shouldTakeUmbrella(weatherDescription));
        } catch (ServiceClientErrorException e) {
            LOGGER.error(e);
            throw new ServiceErrorException("An error occurred or wrong params", e);
        } catch (ServiceClientNoDataFoundException e) {
            throw new NoDataFoundException(format("Location %s not found", location), e);
        } catch (JSONException e) {
            LOGGER.error(e);
            throw new ServiceErrorException("An error occurred or wrong params", e);
        }
        if (weather != null) {
            // Store result asynchronously
            // Normally I won't do it this way, I would rather capture the response
            // or do it some other way that doesn't create coupling between WeatherService and weatherForecastService
            weatherForecastService.addWeatherData(location, weather);
        }
        return weather;
    }
}
