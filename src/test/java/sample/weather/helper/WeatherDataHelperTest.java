package sample.weather.helper;

import org.junit.Test;
import sample.weather.dto.WeatherHistoricalDTO;
import sample.weather.dto.WeatherSummaryDTO;
import sample.weather.model.Weather;
import sample.weather.model.WeatherHistoricalInfo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class WeatherDataHelperTest {

    @Test
    public void convertToDto() {

        WeatherSummaryDTO weatherSummaryDTOExpected = new WeatherSummaryDTO();
        weatherSummaryDTOExpected.setTemp(1);
        weatherSummaryDTOExpected.setPressure(2);
        weatherSummaryDTOExpected.setUmbrella(true);
        Weather weather = new Weather(1.1, 2.1, true);
        WeatherSummaryDTO weatherSummaryDTO = WeatherDataHelper.convertToDto(weather);
        assertEquals(weatherSummaryDTOExpected, weatherSummaryDTO);
    }

    @Test
    public void testConvertToDto() {

        List<Weather> weatherList = Arrays.asList(
                new Weather(1.1, 2.1, true),
                new Weather(1.1, 2.1, true));
        WeatherHistoricalDTO weatherHistoricalDTOExpected = new WeatherHistoricalDTO();
        weatherHistoricalDTOExpected.setAvgTemp(1);
        weatherHistoricalDTOExpected.setAvgPressure(2);
        weatherHistoricalDTOExpected.setHistory(weatherList.stream()
                .map(WeatherDataHelper::convertToDto).collect(Collectors.toList()));
        WeatherHistoricalInfo weatherHistoricalInfo =
                new WeatherHistoricalInfo(1.1, 2.1, weatherList);
        WeatherHistoricalDTO weatherHistoricalDTO = WeatherDataHelper.convertToDto(weatherHistoricalInfo);
        assertEquals(weatherHistoricalDTOExpected, weatherHistoricalDTO);
    }
}