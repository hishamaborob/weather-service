package sample.weather.repository;

import org.junit.Test;
import sample.weather.model.Weather;

import java.util.List;

import static org.junit.Assert.*;

public class WeatherHistoricalInfoRepositoryInMemoryTest {

    @Test
    public void test() {

        final int maxSize = 2;
        WeatherHistoricalInfoRepository weatherHistoricalInfoRepository =
                new WeatherHistoricalInfoRepositoryInMemory(maxSize);
        final String location = "petra";
        weatherHistoricalInfoRepository.add(location, new Weather(30, 50, false));
        weatherHistoricalInfoRepository.add(location, new Weather(31, 54, false));
        weatherHistoricalInfoRepository.add(location, new Weather(33, 50, false));
        List<Weather> weatherList = weatherHistoricalInfoRepository.get(location);
        assertNotNull(weatherList);
        assertFalse(weatherList.isEmpty());
        assertEquals(maxSize, weatherList.size());
    }
}