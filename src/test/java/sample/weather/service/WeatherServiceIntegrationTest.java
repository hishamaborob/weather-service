package sample.weather.service;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import sample.weather.client.WeatherServiceClient;
import sample.weather.model.Weather;
import sample.weather.model.WeatherHistoricalInfo;
import sample.weather.repository.WeatherHistoricalInfoRepositoryInMemory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;

public class WeatherServiceIntegrationTest {

    private MockWebServer mockWebServer;

    private WeatherService weatherService;
    private WeatherForecastService weatherForecastService;

    @Before
    public void setUp() throws Exception {

        final String mockApiResponse1 = readFile("mockApiResponse1.json");
        final String mockApiResponse2 = readFile("mockApiResponse2.json");
        final String contentTypeHeader = "Content-Type";
        final String contentType = "application/json; charset=utf-8";
        final int responseCode = 200;
        mockWebServer = new MockWebServer();
        mockWebServer.enqueue(new MockResponse()
                .addHeader(contentTypeHeader, contentType)
                .setResponseCode(responseCode)
                .setBody(mockApiResponse1));
        mockWebServer.enqueue(new MockResponse()
                .addHeader(contentTypeHeader, contentType)
                .setResponseCode(responseCode)
                .setBody(mockApiResponse1));
        mockWebServer.enqueue(new MockResponse()
                .addHeader(contentTypeHeader, contentType)
                .setResponseCode(responseCode)
                .setBody(mockApiResponse2));
        mockWebServer.start();
        HttpUrl baseUrl = mockWebServer.url("/");
        weatherForecastService =
                new WeatherForecastService(new WeatherHistoricalInfoRepositoryInMemory(2),
                        Executors.newSingleThreadExecutor());
        weatherService = new WeatherService(
                new WeatherServiceClient(new OkHttpClient(), baseUrl.url().toString(), ""),
                new UmbrellaService(), weatherForecastService);
    }

    @Test
    public void getCurrentWeather() {

        Weather weatherExpected1 = new Weather(10.0, 1008, false);
        Weather weatherExpected2 = new Weather(12.0, 1010, false);
        WeatherHistoricalInfo weatherHistoricalInfoExpected = new WeatherHistoricalInfo(
                11.0, 1009,
                Arrays.asList(weatherExpected1, weatherExpected2)
        );

        final String location = "berlin";
        Weather weather = null;

        weather = weatherService.getCurrentWeather(location);
        assertNotNull(weather);
        assertEquals(weatherExpected1, weather);

        weather = weatherService.getCurrentWeather(location);
        assertNotNull(weather);
        assertEquals(weatherExpected1, weather);

        weather = weatherService.getCurrentWeather(location);
        assertNotNull(weather);
        assertEquals(weatherExpected2, weather);

        WeatherHistoricalInfo weatherHistoricalInfo = weatherForecastService.getWeatherHistoricalData(location);
        assertNotNull(weatherHistoricalInfo);
        assertEquals(weatherHistoricalInfoExpected, weatherHistoricalInfo);
    }

    @After
    public void tearDown() throws Exception {
        mockWebServer.shutdown();
    }

    public String readFile(String filename) throws URISyntaxException, IOException {

        URL resource = this.getClass().getClassLoader().getResource(filename);
        byte[] bytes = Files.readAllBytes(Paths.get(resource.toURI()));
        return new String(bytes);
    }
}